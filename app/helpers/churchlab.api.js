(function () {

    var httpRequest = require(__dirname + '/http.request'),
    fs = require('fs'),
    config = JSON.parse(fs.readFileSync(__dirname + '/../config.json', 'utf8'))

    var base = 'https://churchlabapp.com/serverapi/v1';
    var auth = {
        api_token: config.cl_api_token,
        token: config.cl_api_key,
        t: config.cl_team_id
    }

    // current route list:
    // /recording/serverRecordDone (recordings.js)
    // /streaming/serverPushConfig (relays.js)
    // /streaming/serverPublishStart (streams.js)
    // /streaming/serverPublishEnd (streams.js)


    var post = function (url, data, headers, success, error) {


        var paramData = auth;

        if(data){
            paramData = Object.assign(auth, data);
        }

        var api = base + url;

        // console.log('api call (post):');
        // console.log(api);
        // console.log(paramData);

        httpRequest.post(api, paramData, headers, function (result) {

            success(result);

        }, function (e){

            error(e);

        });

    };

    var get = function (url, params, headers, success, error) {
     
        var paramData = auth;

        if(params){
            paramData = Object.assign(auth, params);
        }

        var api = base + url;

        httpRequest.get(api, paramData, headers, function (result) {

            success(result);

        }, function (e){

            error(e);

        });

    };

   
    module.exports = {
        post: post,
        get: get
    };

})();