(function () {

    var http = require('follow-redirects').http;
    var URL = require('url');
    var querystring = require('querystring');

    var post = function (url, data, headers, success, error) {
        var parsedUrl = URL.parse(url),
            contentType,
            dataStr;

        if (!headers['Content-Type']) {
            headers['Content-Type'] = 'application/x-www-form-urlencoded';
        }

        contentType = headers['Content-Type'];

        if (contentType == 'application/json') {
            dataStr = JSON.stringify(data);
        } else {
            dataStr = querystring.stringify(data);
            headers['Content-Length'] = dataStr.length;
        }

        var request = http.request({
            hostname: parsedUrl.hostname,
            path: parsedUrl.path,
            method: 'POST',
            headers: headers

        }, function (response) {

            var output = '';
            response.setEncoding('utf8');

            response.on('data', function (chunk) {
                output += chunk;
            });

            response.on('end', function () {
                var result = JSON.parse(output);

                success(result);

            });

        });

       // console.log(dataStr);

        request.write(dataStr);
        request.end();
    };

    var get = function (url, params, headers, success, error) {
        var parsedUrl = URL.parse(url),
            paramStr = querystring.stringify(params);

        headers = headers || {};

        var request = http.request({
            hostname: parsedUrl.hostname,
            path: parsedUrl.path + '?' + paramStr,
            method: 'GET',
            headers: headers

        }, function (response) {

            var output = '';
            response.setEncoding('utf8');

            response.on('data', function (chunk) {
                output += chunk;
            });

            response.on('end', function () {
                var result;

                try {
                    result = JSON.parse(output);
                    success(result);
                }
                catch(e) {
                    if (typeof error === 'function') {
                        error(e);
                    }
                }                
            });
        });

        request.end();
    };

    module.exports = {
        post: post,
        get: get
    };

})();