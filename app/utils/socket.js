const Server = require('socket.io');
const io = new Server({'path' : '/app/socket'});

var Socket = {
    emit: function (event, data) {
        // console.log(event, data);
        io.sockets.emit(event, data);
    }
};

io.on("connection", function (socket) {
    // console.log("A user connected.");
    // console.log("ip: "+socket.handshake.headers['x-forwarded-for'].split(',')[0]);
    // console.log("user-agent: "+socket.request.headers['user-agent']);
});

exports.Socket = Socket;
exports.io = io;