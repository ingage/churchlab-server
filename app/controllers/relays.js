var express = require('express'),
router = express.Router(),
api = require(__dirname + '/../helpers/churchlab.api'),
http = require('http'),
io = require('socket.io'),
socket = io();

const { Socket } = require('../utils/socket');


//////////////////
//  YOUTUBE, FACEBOOK, AND EMBEDS PUSH
//////////////////



router.get('/app/relay-started', function(req, res){

   console.log('relay started');

   let relay_id = req.query['relay_id'];
   var client_id = req.query['clientid'];

   if(!relay_id){
        res.sendStatus(200); 
        return;
    }

    api.get(`/relays/${relay_id}/server-started`, {  client_id: client_id }, null, function (result) {

        res.sendStatus(200)

    });


});


router.get('/app/push/facebook', function(req, res){

    // console.log(req.query);

    // console.log('calling home to check facebook');

    api.get('/streaming/facebook/config', false, null, function (result) {

            console.log('Push Config [Facebook]');
            console.log(result);

            var fb_enabled = result.facebook.enabled;
            var fb_url = result.facebook.url;


            if(fb_enabled && fb_url != ""){
               // console.log('redirecting for facebook!');
                res.set('X-RTMP-TC-URL', 'rtmp://live-api-a.facebook.com/');
                return res.redirect(301, fb_url);

            } else {

               // console.log('call succeeded: not pushing');
                res.sendStatus(200);
                return;
            }

    }, function (error){
            // console.log('call error: not redirecting for facebook');
            // console.log(error);
            res.sendStatus(400);
            return;

    });




});


router.get('/app/push/youtube', function(req, res){

    // console.log(req.query);

    api.get('/streaming/youtube/config', false, null, function (result) {
        
            console.log('Push Config [Youtube]');
            console.log(result);

            var yt_enabled = result.youtube.enabled;
            var yt_url = result.youtube.url;
            
            if(yt_enabled && yt_url != ""){
             //   console.log('redirecting for youtube!');
                res.set('X-RTMP-TC-URL', 'rtmp://live-api-a.facebook.com/');
                return res.redirect(301, yt_url);

            } else {

               // console.log('call succeeded: not pushing to youtube');
                res.sendStatus(200);
                return;
            }

    }, function (error){
            // console.log('call error: not redirecting for youtube');
            // console.log(error);
            res.sendStatus(400);
            return;

    });




});


router.get('/app/push/embeds', function(req, res){

    // console.log(req.query);

    api.get('/streaming/embeds/config', false, null, function (result) {

            console.log('Push Config [Embeds]');
            console.log(result);

            var embeds_enabled = result.embeds.enabled;

            // var embeds_enabled = true;

            var embeds_url = result.embeds.url;
             
            if(embeds_enabled){
             

                console.log('embeds enabled: redirecting to ' + embeds_url);

                // socket.emit('stream_event', { event : 'started' });

                return res.redirect(301, embeds_url);             
         

            } else {

                console.log('call succeeded: embeds disabled');
                res.sendStatus(200);
                return;
            }

    }, function (error){
            // console.log('call error: not redirecting for youtube');
            // console.log(error);
            res.sendStatus(400);
            return;

    });




});


router.get('/app/push/embeds/auth', function(req, res){

//    console.log(req)

    var legit = req.query['name'];

    console.log('Authing embeds')
    
    var key = config.cl_stream_key;

    if(legit != key){
        console.log('Embeds Stream Rejected (key)')
        res.sendStatus(401);
    }

    console.log('Embeds Stream Started');

    Socket.emit('stream_event', { event : 'started' });

    var embeds_url = req.query['embeds_url'];

    console.log('redirecting to ' + embeds_url);

    return res.redirect(301, embeds_url);

});



router.get('/app/push/private', function(req, res){

    // console.log(req.query);
    // console.log('calling home to check youtube');

    api.get('/streaming/private/config', false, null, function (result) {

        console.log('Push Config [Private]');
        console.log(result);

        var private_enabled = result.private.enabled;
        var private_url = result.private.url;

        if(private_enabled){

            console.log('private stream: redirecting to ' + private_url);
            return res.redirect(301, private_url);             

        } else {

            console.log('call succeeded: not private');
            res.sendStatus(200);
            return;
        }

    }, function (error){

        // console.log('call error: not redirecting for youtube');
        // console.log(error);
        res.sendStatus(400);
        return;

    });

});


router.get('/app/push/private/auth', function(req, res){

    //    console.log(req)
    
        var legit = req.query['name'];
    
        console.log('Authing private')
        
        var key = config.cl_stream_key;
    
        if(legit != key){
            console.log('Private Stream Rejected (key)')
            res.sendStatus(401);
        }
    
        console.log('Private Stream Started');
    
        Socket.emit('stream_event', { event : 'started' });
    
        var private_url = req.query['private_url'];
    
        console.log('redirecting to ' + private_url);
    
        return res.redirect(301, private_url);
    
    });




router.get('/app/push/facebook/play', function(req, res){

    console.log('Facebook [PLAY]');
    res.sendStatus(200);

});

router.get('/app/push/facebook/play_done', function(req, res){

    console.log('Facebook [DONE]');

    api.get('/streaming/facebook/done', false, null, function (result) {
        res.sendStatus(200);
    });

});

router.get('/app/push/youtube/play', function(req, res){
    console.log('Youtube [PLAY]');
    res.sendStatus(200);
});

router.get('/app/push/youtube/play_done', function(req, res){

    // console.log(req.query);

    console.log('Youtube [DONE]');

    api.get('/streaming/youtube/done', false, null, function (result) {

        res.sendStatus(200);

    });


});





module.exports = router;