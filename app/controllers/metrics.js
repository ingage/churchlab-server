var express = require('express'),
router = express.Router(),
fs = require('fs'),
fsx = require('fs-extra'),
config = JSON.parse(fs.readFileSync(__dirname + '/../config.json', 'utf8')),
os = require('os'),
geoip = require('geoip-lite'),
useragent = require('useragent'),
storage = require('node-persist'),
NginxParser = require('nginxparser'),
strptime = require('micro-strptime').strptime,
parser = new NginxParser('$remote_addr - $remote_user [$time_local] '
        + '"$request" $status $body_bytes_sent "$http_referer" "$http_user_agent"')

function getKeyByValue(object, value) {
    return Object.keys(object).find(key => object[key] === value);
}


router.get('/app/session/info', function (req, res) {


    storage.init({logging: false, forgiveParseErrors: true}).then(function() {

        var keys = storage.keys();

        var info = [];

        for (var i = keys.length - 1; i >= 0; i--) {

            var k = keys[i];

            if(k != "live"){

                storage.getItem(k).then(function(sessionVal){

                    var kv = {
                        name : k,
                        value: sessionVal
                    }

                    info.push(kv);

                })

            }

        }


        res.send(info)


    });

});



router.get('/app/session/started', function (req, res) {

    var sessionStarted = req.query['session'];

    storage.init({logging: false, forgiveParseErrors: true}).then(function() {
        //then start using it
        storage.getItem('session').then(function(sessionVal){

            var storedSession = sessionVal;



            if(sessionStarted != storedSession){

                storage.setItem("session", sessionStarted);

            }

            fs.truncate('/usr/local/nginx/logs/access.log', 0, function(){
                console.log('Log cleared on session start.')         
            });


            res.send({ success : true})


        });

    });

});


router.get('/app/session/ended', function(req, res){

    var now = Date.now();

    var sessionEnded = req.query['session'];

    var sessionEndedString = "session"+sessionEnded.toString();

    storage.init({logging: false, forgiveParseErrors: true}).then(function() {

        storage.getItem("live").then(function(liveVal){

            if(liveVal == "yes"){

                storage.getItem("session").then(function(sessionVal){

                    if(sessionVal && sessionVal != ""){

                        var fname = 'session-'+now+'.log';
    
                        var nameString = sessionVal.toString();
                        var storageName = "session" + nameString;
    
                        fsx.copy('/usr/local/nginx/logs/access.log', '/root/logs/' + fname, function(){ 
                            
                            storage.getItem(storageName).then(function(storedVal){
    
                                if(storedVal && storedVal.logs){
    
                                    storedVal.logs.push(fname);
                                    storage.setItem(storageName, storedVal);
    
                                } else {
    
                                    var save = {
                                        logs : [fname]
                                    }
    
                                    storage.setItem(storageName, save);
    
                                }
    
                                storage.getItem(sessionEndedString).then(function(sessionLogs){

                                    if(sessionLogs){
                                        res.send(sessionLogs);
                                    } else {
                                        res.send({ logs : false})
                                    }
            
                                }) 
                            
                            })
    
                        });
    
                        
    
                    } else {

                        storage.getItem(sessionEndedString).then(function(sessionLogs){

                            if(sessionLogs){
                                res.send(sessionLogs);
                            } else {
                                res.send({ logs : false})
                            }
    
                        }) 
                    
                    }


                })

            } else {

                    
                storage.getItem(sessionEndedString).then(function(sessionLogs){

                    if(sessionLogs){
                        res.send(sessionLogs);
                    } else {
                        res.send({ logs : false})
                    }

                })  


            }

        })

    })

});

router.get('/app/session/stored', function(req, res){

 
    storage.init({logging: false, forgiveParseErrors: true}).then(function() {

        res.send(storage.keys());

    });

});

router.get('/app/session/logs', function(req, res){


    var session = req.query['session'];

    storage.init({logging: false, forgiveParseErrors: true}).then(function() {

        var s = "session" + session.toString();

        storage.getItem(s).then(function(sessionLogs){

            if(sessionLogs){
                res.send(sessionLogs);
            }else {
                res.send({logs : false});
            }

        })

    });

});

function processLogFile(file){

    return new Promise(resolve => {

        var rows = [];

        parser.read(file, function (row) {

          rows.push(row);

            // end parse read SUCCESS  
            // begin parse read ERROR / END
        }, function (err) {

            if (err) {
                console.log(rows);
                throw err;
            }

            console.log('done with file ' + file);

            resolve(rows);
        }); // end parse read ERROR / END


    })

 


}
 


router.get('/app/session/parse', function (req, res){


    // uncomment for parsing

    var sessionToParse = req.query['session'];
    var sessionKey = "session"+sessionToParse.toString();

    storage.init({logging: false, forgiveParseErrors: true}).then(function() {

        storage.getItem(sessionKey).then(function(sessionLogs){


            /* FOR TESTING */

            // var sessionLogs = {
            //     logs : [
            //         "session-1531068319543.log",
            //         "session-1531068333879.log",
            //         "session-1531068344073.log",
            //         "session-1531069728186.log"
            //     ]
            // }

            /* END FOR TESTING */

            if(sessionLogs && sessionLogs.logs && sessionLogs.logs.length){

                var finalItemsResponse = {error: false, files : sessionLogs.logs.length, metrics : {}};

                var totalRows = [];
                var endOfLogs = sessionLogs.logs.length - 1;

                function processRowsAndComplete(){

                    var finalNum = 0;


                    // start = start of stream
                    // end = end of stream
                    // totalIps = total of unique ips
                    // totalDevices = total of unique ip + device combos - i.e. "users"
                    // locations = unique locations and total amount of viewers
                    // platforms = unique platforms and total amount of viewers
                    // browsers = unique browsers and total amount of viewers
                    // ips = list of ips
                    // timeline = number of users at each time interval

                    var items = {
                        start: null,
                        end: null,
                        totalIps: 0,
                        totalDevices: 0,
                        locations: [],
                        platforms: [],
                        browsers: [],
                        ips: [],
                        timeline: []
                    };
             
                    var ips = [];
             
                    var devices = [];
             
                    var users = [];
             
                    var usersInfo = [];
             
                    var ipsInfo = [];
             
                    var locations = [];
                    var platforms = [];
                    var browsers = [];
             
             
             
                    var currentTime = null;
             
                    var currentTimeUsers = [];
             
             
                    var i = 0;

                    var rp;

                    for(rp = 0; rp < totalRows.length; rp++){

                        var row = totalRows[rp];
                       // console.log(row);

                        var time = row.time_local;

                        var cleanTime = strptime(time, '%d/%B/%Y:%H:%M:%S %Z');
            
                        var timestamp = new Date(cleanTime).getTime();
                        timestamp = timestamp / 1000;
            
                        // exlude tracking m3u8 and preview files, only tracking .ts hits, which indicates playing
                        // CORRECTION: this actually is tracking m3u8, not .ts
                        var r = row.request;
                        var n = r.search("m3u8");
                        var p = r.search("preview");
                        var status = row.status;
                        if(n > -1 && status == "200" && p == -1){
            
                            // track if this time is the beginning or the end of stream
                            if(i > 0){
                                items.end = timestamp;
                            } else {
                                items.start = timestamp;
                            }

                            // everything based on ips first
                            // determines IP location
                            // if IP hasn't been identified yet, determine geo and save
                            // otherwise, do nothing because we already know IP location details
            
            
                            if(!ips.includes(row.ip_str)){
                                ips.push(row.ip_str);
            
                                var geo = geoip.lookup(row.ip_str);
            
                                var ipDetails = {
                                    devices: [],
                                    geo: {
                                        "cou" : geo.country,
                                        "reg": geo.region,
                                        "cit": geo.city,
                                        "ll" : geo.ll,
                                        "zip" : geo.zip
                                    }
                                }  
            
                                ipsInfo.push(ipDetails);
            
                                // create counts of ips in areas
                                
                                // create geoString for unique locations list & no of users
                                
                                var geoString = "";

                                if(geo.city != ""){
                                    geoString = geo.city;
                                }

                                if(geo.region != ""){
                                    if(geoString == ""){
                                        geoString = geo.region;
                                    } else {
                                        geoString = geoString + " " + geo.region;
                                    }
                                }

                                if(geo.country != ""){
                                    if(geoString == ""){
                                        geoString = geo.country;
                                    } else {
                                        geoString = geoString + " " + geo.country;
                                    }
                                }

                            
            
                                // if location not added, add it
                                // else, increment the count of users from location

                                if(!locations.includes(geoString)){
                                    locations.push(geoString);
            
                                    var locationData = {
                                        name: geoString,
                                        ips: 1
                                    }
            
                                    items.locations.push(locationData);
                                
                                } else {
            
                                    var geoKey = getKeyByValue(locations, geoString);
                                    items.locations[geoKey].ips++;
            
                                }
            
                            }
            
                            var currentIPKey = getKeyByValue(ips, row.ip_str);

                            // create unique string for identifying user
            
                            var userString = row.ip_str + " // " + row.http_user_agent;

                            // if user not identified, save details and update platforms/agents
                            // else, update the duration of viewing for the user
            
                            if(!users.includes(userString)){

                                users.push(userString);
            
                                var agent = useragent.lookup(row.http_user_agent);
            
                                var userDetails = {
                                    // referrer: row.http_referer,
                                    user_agent: row.http_user_agent,
                                    start: timestamp,
                                    //  end: timestamp,
                                    duration: 0,
                                    agent: agent.family,
                                    os: agent.os.toJSON().family,
                                    //agent_device: agent.device.toJSON().family
                                }
            
                                ipsInfo[currentIPKey].devices.push(userDetails);
    

                                // track counts on platforms
                                // if not added, add it
                                // else, increment the count of users on platform

                                if(!platforms.includes( agent.os.toJSON().family )){
                                    platforms.push( agent.os.toJSON().family );
                                    
                                    var platformData = {
                                        name: agent.os.toJSON().family,
                                        devices: 1
                                    }
            
                                    items.platforms.push(platformData);
                                
                                } else {
            
                                    var platformKey = getKeyByValue(platforms, agent.os.toJSON().family);
                                    items.platforms[platformKey].devices++;
            
                                }
            
            
                                // track counts on agents
                                // if not added, add it
                                // else, increment the count of users on user agent
                                if(!browsers.includes( agent.family )){
                                    browsers.push( agent.family );
                                    var browserData = {
                                        name: agent.family,
                                        devices: 1
                                    }
            
                                    items.browsers.push(browserData);
                                
                                } else {
            
                                    var browserKey = getKeyByValue(browsers, agent.family);
                                    items.browsers[browserKey].devices++;
            
                                }
            
                            
            
                            } else {
            
                                for (var z = ipsInfo[currentIPKey].devices.length - 1; z >= 0; z--) {
            
                                    if(ipsInfo[currentIPKey].devices[z].user_agent == row.http_user_agent){
                                        var duration = timestamp - ipsInfo[currentIPKey].devices[z].start;
                                        // ipsInfo[currentIPKey].devices[z].end = timestamp;
                                        ipsInfo[currentIPKey].devices[z].duration = duration;
                                        break;
                                    }
                                }
                            }

                            // create/update timeline
                            // timeline only updated every 30s
            
                            if(timestamp == currentTime){
                                // add to array
                                var x = i-1;
                                items.timeline[x].users++;
                            } else {
                                var diff = timestamp - currentTime;
                                
                                if(diff >= 60){
                                    // add a new row to the timeline.
                                    // rows are made every 30 seconds
                                    // reset the row user tracker
                                    currentTime = timestamp;
                                    items.timeline[i] = {
                                        time: timestamp,
                                        users: 1
                                    };
                                    
                                    currentTimeUsers = [];
                                    currentTimeUsers.push(userString);
                                    i++;
                                } else {
                                    // increment the number if the current user
                                    // has not already been tracked in this row
                                    if(!currentTimeUsers.includes(userString)){
                                        currentTimeUsers.push(userString);
                                        var x = i-1;
                                        items.timeline[x].users++;
                                    }
                                }
                            }
            
                            finalNum++;

                        } // end if status is OK

                    } // END FOREACH TOTALROWS

                    /////
                    ///// CLEAN UP DEVICE USER AGENT INFO THAT WE DONT NEED TO SAVE
                    /////

                    for (var i = ipsInfo.length - 1; i >= 0; i--) {


                        for (var z = ipsInfo[i].devices.length - 1; z >= 0; z--) {

                            delete ipsInfo[i].devices[z].user_agent;

                            
                        }

                    }

                    //////
                    ////// SAVE PROCESSED DATA TO JSON & RESPOND
                    //////


                    items.totalIps = ips.length;
                    items.totalDevices = users.length;
                    items.ips = ipsInfo;

                    finalItemsResponse.metrics = items;

                    var savedOutputFile = '/root/logs-parsed/log-'+sessionToParse+'.json';

                    fsx.outputJSON(savedOutputFile, finalItemsResponse, function(err){ 
                        
                        if(err){
                            console.log(err);
                        } else {
                            console.log('successfully wrote parsed log to file ' + savedOutputFile);
                        }
                    })


                    res.send(finalItemsResponse);

                }



                function processFileAndProceed(ind){

                    // console.log(sessionLogs);

                    var logFileName = sessionLogs.logs[ind];

                    var path = '/root/logs/' + logFileName;

                    processLogFile(path).then(function(results){

                        var newTotal = totalRows.concat(results);
                        totalRows = newTotal;

                        if(ind != endOfLogs){
                            var next = ind + 1;
                            processFileAndProceed(next);
                        } else {
                           // alert("Done");

                            processRowsAndComplete();
                        }

                    })


                }


                var checkForSavedOutputFile = '/root/logs-parsed/log-'+sessionToParse+'.json';

                fsx.readJson(checkForSavedOutputFile, function (err, data) {
                    if (err) {
                        processFileAndProceed(0);
                        return;
                    }

                    res.send(data);                
                });

            // old process of not saving to file
            // processFileAndProceed(0);


            // end check for sessionKey.logs
            } else {

                res.send({'error': true});

            }


       }) // end get session logs


   }) // end init storage



});


router.get('/app/session/logs/clean', function (req, res){


    fsx.emptyDirSync('/root/logs');
    fsx.emptyDirSync('/root/logs-parsed')


});



module.exports = router;