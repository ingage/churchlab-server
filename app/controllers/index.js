var express = require('express'),
router = express.Router()


// app.all('/*', function(req, res, next) {
    
// });

router.use(function (req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "X-Requested-With");
    res.header('Cache-Control', 'private, no-cache, no-store, must-revalidate');                
    next();
})

router.use(require('./relays'))
router.use(require('./recordings'))
router.use(require('./utils'))
router.use(require('./streams'))
router.use(require('./info'))
router.use(require('./metrics'))
router.use(require('./delivery'))
router.use(require('./simlive'))




module.exports = router;