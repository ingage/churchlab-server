var express = require('express'),
router = express.Router(),
fs = require('fs'),
config = JSON.parse(fs.readFileSync(__dirname + '/../config.json', 'utf8')),
// awsCli = require('aws-cli-js'),
// Options = awsCli.Options,
// Aws = awsCli.Aws,
storage = require('node-persist'),
AWS = require('aws-sdk'),
ffmpeg = require('fluent-ffmpeg'),
api = require(__dirname + '/../helpers/churchlab.api'),
path = require('path');



ffmpeg.setFfmpegPath('/usr/bin/ffmpeg')

//////////////////
//  RECORDING FUNCTIONS
//////////////////

router.get('/app/record/redirect', function(req, res){

  
    api.get('/stream-record/config', false, null, function (result) {

        console.log('Record Config');
        console.log(result);

        var record_url = result.url;
        
        if(result.central){
            console.log('central recording enabled');
        } else {
            console.log('central recording disabled. recording locally');
        }

        return res.redirect(301, record_url);

    }, function (error){
            console.log('error retrieving recording config');
            console.log(error);
            res.sendStatus(400);
            return;
    });

});

router.get('/app/recordings', function(req, res){

    const folder = '/video_recordings/';

    fs.readdir(folder, (err, files) => {

        let filesArray = [];

        if(err || files.length == 0){
            res.send([]);
        }

        for (let index = 0; index < files.length; index++) {

            let filePath = files[index];

            if(fs.lstatSync(folder + filePath).isDirectory()){
                continue;
            }

            // let fileName = path.basename(filePath)

            // let file = {
            //     name: fileName,
            //     path: filePath
            // }

            filesArray.push(filePath);
            
        }
        

        res.send(filesArray);

    })

});

router.get('/app/recordings/converted', function(req, res){

    const folder = '/video_recordings/converted/';

    fs.readdir(folder, (err, files) => {

        let filesArray = [];

        if(err || files.length == 0){
            res.send([]);
            return;
        }

        for (let index = 0; index < files.length; index++) {

            let filePath = files[index];

            if(fs.lstatSync(filePath).isDirectory()){
                continue;
            }

            // let fileName = path.basename(filePath)

            // let file = {
            //     name: fileName,
            //     path: filePath
            // }

            filesArray.push(filePath);
            
        }
        

        res.send(filesArray);

    })


});

router.get('/app/convert', function(req, res){

    // console.log(req.query);

    var recording_file_path = req.query['path'];
    // var finalize_filename_debug = req.query['final_name'];

    console.log('conversion triggered. path: ' + recording_file_path);
    

    const stats = fs.statSync(recording_file_path);
    const fileSizeInBytes = stats.size;

    // 35mb threshold for saving files
    if(fileSizeInBytes < 35000000){

         fs.unlink(recording_file_path, function(error){
             if (error) {
                 console.log('error deleting file: ' + error.message);
             } else {
                 console.log('file ' + recording_file_path + ' was not large enough and was deleted.');
             }

             res.end()

         })

         return;

    } 

     var params = {
         path: recording_file_path
     }

     
     api.get('/recording/serverRecordDone', params, null, function (result) {

         console.log('finalize name returned: ' + result.name);
  
         // var recording_file = req.query['path'];
         var finalize_filename = result.name;
         var recording_id = result.id;

         var storage = result.storage;
         var storage_bucket = result.storage_bucket;
         var storage_folder = result.storage_folder;


         if(recording_file_path && finalize_filename){

             if(fs.existsSync(recording_file_path)){

                 var output_file = '/video_recordings/converted/'+finalize_filename+'.mp4';
                 var command = ffmpeg();

                 command
                     .input(recording_file_path)
                     .addOption('-y')
                     .addOption('-codec', 'copy')
                     .addOption('-movflags')
                     .addOption('+faststart')
                     .on('end', function(){

                        console.log('file converted successfully');

                        /* delete original file */
                        fs.unlink(recording_file_path, function(error){
                            if (error) {
                                console.log('error deleting original file: ' + error.message);
                            } else {
                                console.log('successfully deleted original file!');
                            }

                        })


                        setS3Config(storage);
                        
                        var s3 = new AWS.S3({
                            apiVersion: "2006-03-01",
                        });

                         var fileKey = storage_folder + '/' + finalize_filename+'.mp4';

                         // Use S3 ManagedUpload class as it supports multipart uploads
                        var upload = new AWS.S3.ManagedUpload({
                            params: {
                                Bucket: storage_bucket,
                                Key: fileKey,
                                Body: fs.createReadStream(output_file),
                                ACL: "public-read",
                                ContentType: 'video/mp4'
                            }
                        });

                        upload.send();

                        var promise = upload.promise();

                        promise.then(
                            function(data) {
                                console.log('uploaded to s3 successfully');                         
    
                                api.get(`/recordings/${recording_id}/finalize`, params, null, function (result) {
                                    console.log('finalize triggered');
                                });
                         
                                 /* delete converted file */
                                 fs.unlink(output_file, function(error){
                                     if (error) {
                                         console.log('error deleting converted file: ' + error.message);
                                     } else {
                                         console.log('successfully deleted converted file!');
                                    }

                                    res.end()

                                })


                            },
                            function(err) {
                                console.log('error uploading to s3');
                                res.end();
                            }
                        );

                        upload.on('httpUploadProgress', function (progress) {
                            // document.getElementById("progressNumber").value = (progress.loaded / progress.total) * 100;
                            // var percent = (progress.loaded / progress.total) * 100;
                            // console.log('upload progress:' + percent + '%');
                        })

                     })
                     .on('error', function(err){
                         console.log('conversion error: ' + err.message);
                     })
                     .save(output_file);


                 } else {

                     console.log(recording_file_path + ' attempted conversion but did not exist.');
                      res.end()
                 }

       } else {

             console.log('Conversion requirements not met.');
             res.end()
       }

       // end call for the file name
     }, function (error) {

        console.log('error retrieving finalization file name. error: ');
        console.log(error);

     });

 
  });




router.get('/app/upload-converted', function(req, res){


    // console.log(req.query);
 
    var file = req.query['file'];
    var recording_id = req.query['recording_id'];
    var storage = req.query['storage'];
    var storage_bucket = req.query['storage_bucket'];
    var upload_key = req.query['upload_key'];

    // var finalize_filename_debug = req.query['final_name'];
    
    var saved_file = '/video_recordings/converted/'+file;

    console.log('upload-converted triggered. path: ' + saved_file);


    if(!fs.existsSync(saved_file)){
        console.log(saved_file + ' attempted upload but did not exist.');
        res.end();
    }
                
       
    setS3Config(storage);
                
        
    var s3 = new AWS.S3({
        apiVersion: "2006-03-01",
    });

    var fileKey = storage_folder + '/' + file;

    // Use S3 ManagedUpload class as it supports multipart uploads
    var upload = new AWS.S3.ManagedUpload({
        params: {
            Bucket: storage_bucket,
            Key: upload_key,
            Body: fs.createReadStream(saved_file),
            ACL: "public-read",
            ContentType: 'video/mp4'
        }
    });

    upload.send();

    var promise = upload.promise();

    promise.then(
        function(data) {
            console.log('uploaded to s3 successfully');

            var params = {};

            api.get(`/recordings/${recording_id}/finalize`, params, null, function (result) {
                console.log('finalize triggered');
            });
        
            /* delete saved file */
            fs.unlink(saved_file, function(error){
                if (error) {
                    console.log('error deleting saved file: ' + error.message);
                } else {
                    console.log('successfully deleted saved file!');
                }

            res.end()

        })


        },
        function(err) {
            console.log('error uploading to s3');
            res.end();
        }
    );

 
  

  });

  

  router.get('/app/move', function(req, res){


    // console.log(req.query);
 
    var file = req.query['file'];
    var recording_id = req.query['recording_id'];
    var storage = req.query['storage'];
    var storage_bucket = req.query['storage_bucket'];
    var upload_key = req.query['upload_key'];

    // var finalize_filename_debug = req.query['final_name'];

    
    var saved_file = '/video_recordings/'+file;

    console.log('move triggered. path: ' + saved_file);


    if(!fs.existsSync(saved_file)){
        console.log(saved_file + ' attempted upload but did not exist.');
        res.end();
    }
                
       
    setS3Config(storage);
        
    var s3 = new AWS.S3({
        apiVersion: "2006-03-01",
    });

    // Use S3 ManagedUpload class as it supports multipart uploads
    var upload = new AWS.S3.ManagedUpload({
        params: {
            Bucket: storage_bucket,
            Key: upload_key,
            Body: fs.createReadStream(saved_file),
            ACL: "public-read",
            ContentType: 'video/x-flv'
        }
    });

    upload.send();

    var promise = upload.promise();

    promise.then(
        function(data) {

            console.log('file uploaded successfully');

            var params = {}

            api.get(`/recordings/${recording_id}/move-completed`, params, null, function (result) {
                console.log('move-completed');
            });

            /* delete saved file */
            fs.unlink(saved_file, function(error){
                if (error) {
                    console.log('error deleting saved file: ' + error.message);
                } else {
                    console.log('successfully deleted saved file!');
                }

                res.end()

            })


        },
        function(err) {
            console.log('error uploading to s3');
            res.end();
        }
    );

 
  

  });

  function setS3Config(storage){


    if(storage == "do"){

        AWS.config.update({
            //Censored keys for security
            accessKeyId: config.do_key,
            secretAccessKey: config.do_secret,
            region: 'nyc3',
            endpoint: 'https://nyc3.digitaloceanspaces.com'
        });

    } else {

        AWS.config.update({
            //Censored keys for security
            accessKeyId: config.aws_key,
            secretAccessKey: config.aws_secret,
            region: 'us-east-1',
        });

    }


  }


  module.exports = router;