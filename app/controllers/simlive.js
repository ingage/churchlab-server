var express = require('express'),
router = express.Router(),
ffmpeg = require('fluent-ffmpeg'),
fs = require('fs'),
config = JSON.parse(fs.readFileSync(__dirname + '/../config.json', 'utf8')),
api = require(__dirname + '/../helpers/churchlab.api'),
ffmpegProcess = null;


ffmpeg.setFfmpegPath('/usr/bin/ffmpeg')


//////////////////
//  YOUTUBE, FACEBOOK, AND EMBEDS PUSH
//////////////////


router.get('/app/sim/start', function(req, res){

    var fileInput = req.query['file'];

    var stream_key = config.cl_stream_key;

    ffmpegProcess = ffmpeg();

    ffmpegProcess
    .input(fileInput)
    .inputOption('-re')
    .videoCodec('libx264')
    .addOption('-preset', 'veryfast')
    .addOption('-maxrate', '3000k')
    .addOption('-bufsize', '6000k')
    .addOption('-pix_fmt', 'yuv420p')
    .addOption('-g', '50')
    .audioCodec('copy')
    // .audioChannels(2)
    // .audioBitrate(160)
    // .addOption('-strict', '-2')
    // .addOption('-ar', '44100')
    .addOption('-f', 'flv')
    .on('start', function(){
        console.log('simlive started');
       // res.send({ success : true})
    })
    .on('error', function(err, stdout, stderr){
        console.error('simlive error');
        console.error('Cannot process video: ' + err.message);
    })
    .on('end', function(){
        console.log('simlive ended');
        //res.end();
    })
    .output('rtmp://127.0.0.1/live/' + stream_key)
    .run();

    res.send({ success : true });



});


router.get('/app/sim/stop', function(req, res){

 
 
    if(ffmpegProcess !== null){

        ffmpegProcess.kill();
        ffmpegProcess = null;

    }

    res.send({ success : true });



});


module.exports = router;