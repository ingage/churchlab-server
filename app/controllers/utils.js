var express = require('express'),
router = express.Router();
fs = require('fs'),
config = JSON.parse(fs.readFileSync(__dirname + '/../config.json', 'utf8')),
ffmpeg = require('fluent-ffmpeg');

ffmpeg.setFfprobePath('/usr/bin/ffprobe')


router.get('/app/reload', function(req, res){

    exec('service nginx reload', (err, stdout, stderr) => {

            if(err){
                console.log('error restarting nginx');
                res.send({error: true});
            }else {
                console.log('successfully restarted nginx');
                res.send({error: false});
            }
            res.end();
    });

 
});


router.get('/app/stream/probe', function(req, res){


    var stream_key = config.cl_stream_key;
    var stream_url = 'rtmp://127.0.0.1/live/' + stream_key;

    ffmpeg.ffprobe(stream_url, function(err, metadata) {

        if(err){
            console.log(err);
            res.send({ error : err });
            return;
        }

        res.send({ probe : metadata });

    });
 
 
});




router.get('/app/logs/clear', function (req, res) {


    fs.truncate('/usr/local/nginx/logs/error.log', 0, function(){
        console.log('Nginx error log cleared.')         
    });


    res.send({ success : true})

});





module.exports = router;