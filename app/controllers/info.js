var express = require('express'),
router = express.Router(),
fs = require('fs'),
os = require('os'),
config = JSON.parse(fs.readFileSync(__dirname + '/../config.json', 'utf8'));
const { Socket, io } = require('../utils/socket');

const checkDiskSpace = require('check-disk-space').default;

var sslChecker = require("ssl-checker");

router.get('/app', function (req, res) {

    var package = JSON.parse(fs.readFileSync(__dirname + '/../package.json', 'utf8'));

    var version = {
        "version" : package.version
    }

    res.send(version);

});


router.get('/app/stats', function(req, res){

    const freemem = os.freemem();
    const totmem = os.totalmem();

    var freeMb = freemem / 1000000.0;
    var totMb = totmem / 1000000.0;

    var results = {
        memory: {
            free : freeMb.toString(),
            total: totMb.toString()
        }, 
        disk: {
            free: 0,
            size: 0
        }
    }

    checkDiskSpace('/').then((diskSpace) => {

        results.disk.free = diskSpace.free;
        results.disk.size = diskSpace.size;

        let percent_available = Math.round((diskSpace.free /  diskSpace.size) * 100);
        results.disk.available_percent = percent_available;
    
        res.send(results);
    })

 

  
    // res.send(results);

   //res.send(results);

    return;

});

router.get('/app/viewers', function (req, res) {

    //     var count = Socket.connectedClients();
    
            io.of('/').clients((error, clients) => {
                if (error) throw error;
                // console.log(clients); 
                // => [PZDoMHjiu8PYfRiKAAAF, Anw2LatarvGVVXEIAAAD]
                    return res.send({"viewers" : clients.length});
            });
    
    
    //      console.log('count returned: ' + count);
    
    //     req.server.getConnections((error, count) => res.send({"viewers" : count}));
    
        //  var viewers = {
      //        "viewers" : count
    //      }
    
    //      res.send(viewers);
});


 router.get('/app/emit/test', function (req, res) {

    Socket.emit('stream_event', { event : 'test' });
    res.send('success');

});



router.get('/app/ssl-check', function(req, res){

    var domain = config.cl_team_url;

    // var domain = "crystalbrook.streamsprk.com";
 
    sslChecker(domain).then((details) => {

        res.send(details);

    })



});




module.exports = router;