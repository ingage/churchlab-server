var express = require('express'),
router = express.Router(),
fs = require('fs'),
fsx = require('fs-extra'),
storage = require('node-persist'),
api = require(__dirname + '/../helpers/churchlab.api'),
config = JSON.parse(fs.readFileSync(__dirname + '/../config.json', 'utf8'));

const { Socket } = require('../utils/socket');



router.get('/app/status', function (req, res) {
    
    var status = {}

    storage.init({logging: false, forgiveParseErrors: true}).then(function() {
        //then start using it
        storage.getItem('live').then(function(liveVal){

            status.live = liveVal;

            res.send(status);

        })

    }, function(){
        res.send({"error": "Cannot access storage."});
    });


});

router.get('/app/hls/status', function (req, res) {
    
    var status = {
        'ready': false
    }

    var playlist = '/HLS/live/live/index.m3u8';

    var playlistExists = fs.existsSync(playlist);

    if (playlistExists) {
        status.ready = true;
    } 
    
    res.send(status);

 

});


router.get('/app/preview/status', function (req, res) {
    
    var status = {
        'ready': false
    }

    var playlist = '/HLS/preview/live/index.m3u8';

    var playlistExists = fs.existsSync(playlist);

    if (playlistExists) {
        status.ready = true;
    } 
    
    res.send(status);

 

});

router.get('/app/adaptive/status', function (req, res) {
    
    var status = {
        'ready': false
    }

    var playlist = '/HLS/adaptive/live.m3u8';

    var playlistExists = fs.existsSync(playlist);

    if (playlistExists) {
        status.ready = true;
    } 
    
    res.send(status); 

});

router.get('/app/started', function(req, res){


    var legit = req.query['name'];

    console.log('Stream Received')
    
    var key = config.cl_stream_key;

    if(key != "##TEAM_KEY##" && legit == key ){


        storage.init({logging: false, forgiveParseErrors: true}).then(function() {

        
                api.get('/streaming/serverPublishStart', false, null, function (result) {
                    
                    //then start using it
                    storage.setItem('live','yes');

                    console.log('Stream Started');

                    Socket.emit('stream_event', { event : 'started' });

                    console.log('serverPublishStart Response:');
                    console.log(result);

                    if(result && result.session){

                        storage.getItem('session').then(function(sessionVal){

                            if(sessionVal != result.session){
                                storage.setItem('session', result.session);
                            }

                        });

                    }

                    if(result && result.record_key){
                        console.log('setting record_key to: ' + result.record_key);
                        storage.setItem('record_key', result.record_key);
                    }


                    fs.truncate('/usr/local/nginx/logs/access.log', 0, function(){
                        console.log('Logs cleared')         
                    });

                    res.sendStatus(200)

                                    
                }, function (error){

                    console.error("Could not connect to ChurchLab");
                    
                    res.sendStatus(401)
                    
                });



        }) // end storage init
                                

    } else {

        console.log('Stream Rejected (key)')
        
        res.sendStatus(401);
        
    }
        

 });


 router.get('/app/ended', function(req, res){

        var now = Date.now();

        storage.init({logging: false, forgiveParseErrors: true}).then(function() {
            
            storage.setItem('live','no');


            api.get('/streaming/serverPublishEnd', false, null, function (result) {});

            // httpRequest.get(url, params, null, function (result) {});


            storage.getItem('session').then(function(sessionVal){

                if(sessionVal && sessionVal != ""){

                    var fname = 'session-'+now+'.log';

                    var nameString = sessionVal.toString();
                    var storageName = "session" + nameString;

                    fsx.copy('/usr/local/nginx/logs/access.log', '/root/logs/' + fname, function(){ 
                        

                        storage.getItem(storageName).then(function(storedVal){

                            if(storedVal && storedVal.logs){

                                storedVal.logs.push(fname);
                                storage.setItem(storageName, storedVal);

                            } else {

                                var save = {
                                    logs : [fname]
                                }

                                storage.setItem(storageName, save);

                            }

                            res.sendStatus(200);
                        
                        })

                    });

                 

                }

            });

        });

 });


 module.exports = router;