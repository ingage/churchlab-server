(function () {

    'use strict';

        var express = require('express'),
        cookieParser = require('cookie-parser'),
        http = require('http'),
        app = express(),
        port = 4697,
        server = http.createServer(app),
        ffmpeg = require('fluent-ffmpeg')
        
        const { io }  = require("./utils/socket");
        io.attach(server); 
        

        ffmpeg.setFfmpegPath('/usr/bin/ffmpeg');
        ffmpeg.setFfprobePath('/usr/bin/ffprobe');

  
 
        app.use(cookieParser());


        app.use(function(req, res, next){ //This must be set before app.router
                req.server = server;
                next();
         });

        app.use(require('./controllers'))

        server.listen(port, 'localhost');
})();
