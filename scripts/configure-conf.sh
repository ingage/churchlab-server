#!/bin/bash

DOMAIN=$1

echo "Configuring conf with domain"

sed -i "s/myservername.streamsprk.com/$DOMAIN/g" /root/churchlab/conf/server.conf
sed -i "s/#server_name/server_name/g" /root/churchlab/conf/server.conf
sed -i "s/#ssl_certificate/ssl_certificate/g" /root/churchlab/conf/server.conf
sed -i "s/#ssl_certificate_key/ssl_certificate_key/g" /root/churchlab/conf/server.conf
