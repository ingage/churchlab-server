#!/bin/bash

echo "Adding repository"

yes | add-apt-repository ppa:certbot/certbot

echo "Updating..."

# apt-get update

find /etc/apt/sources.list.d -type f -name '*.list' -exec apt-get update -o Dir::Etc::sourcelist="{}" ';'

echo "Installing certbot"

yes | apt-get install certbot

echo "Certbot installed"; exit 0;