#!/bin/bash

# for some reason there are issues with this install script.
# nginx won't start. so instead im just using a pre-configured template
# server on digitalocean

apt-get -y install build-essential libpcre3 libpcre3-dev libssl-dev zlib1g zlib1g-dev unzip nodejs npm certbot ffmpeg

mkdir -p /HLS/live
mkdir -p /HLS/mobile
mkdir -p /video_recordings/converted
chmod -R 777 /video_recordings

mkdir ~/working
cd ~/working

wget https://github.com/ingageco/nginx-rtmp-module/archive/allow-relay-params.zip
unzip allow-relay-params.zip

wget http://nginx.org/download/nginx-1.16.1.tar.gz
tar -zxvf nginx-1.13.1.tar.gz d defaults

cd nginx-1.16.1
./configure --with-http_ssl_module --with-http_stub_status_module --with-threads --with-file-aio  --with-debug --add-module=../nginx-rtmp-module-allow-relay-params --with-cc-opt="-Wimplicit-fallthrough=0"

make
make install

wget https://raw.github.com/JasonGiedymin/nginx-init-ubuntu/master/nginx -O /etc/init.d/nginx
chmod +x /etc/init.d/nginx
update-rc.d nginx defaults

service nginx start
service nginx stop

cd ~

cp nginx.conf /usr/local/nginx/conf/nginx.conf

service nginx restart