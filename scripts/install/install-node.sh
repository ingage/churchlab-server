#!/bin/bash

# curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh -o install_nvm.sh | bash
# chmod +x install_nvm.sh
# bash install_nvm.sh

curl -sL https://raw.githubusercontent.com/creationix/nvm/v0.31.0/install.sh -o install_nvm.sh

chmod +x install_nvm.sh

bash install_nvm.sh

source ~/.nvm/nvm.sh
source ~/.profile
source ~/.bashrc

nvm install 6.11.1

nvm use 6.11.1

npm install -g pm2

cd /root/app/

npm install

pm2 start index.js --watch