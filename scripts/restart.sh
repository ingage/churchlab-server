#!/bin/bash

NODE_VERSION=$1

PATH=$PATH:/root/.nvm/versions/node/$NODE_VERSION/bin

echo "Restarting Node app and Nginx"

echo "Changing to app directory"

cd /root/churchlab/app

echo "Stopping and Removing PM2 app"

pm2 stop all
pm2 delete all
pm2 flush

echo "Starting app"

npm run start

echo "Restarting Nginx"

service nginx restart

echo "App restart completed successfully"; exit 0