#!/bin/bash


echo "Running app update..."

cd /root/churchlab

mkdir -p /root/churchlab/conf

cp -r /root/churchlab/cl-conf/nginx/* /root/churchlab/conf/

cp /root/churchlab/cl-conf/nginx.conf /usr/local/nginx/conf/
cp /root/churchlab/cl-conf/html/index.html /usr/local/nginx/html/

mkdir -p /var/www/letsencrypt
mkdir -p /HLS/preview
mkdir -p /HLS/private
mkdir -p /HLS/private/live
mkdir -p /HLS/private/mobile

exit 0