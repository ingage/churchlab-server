#!/bin/bash

TEAM_ID=$1
TEAM_SLUG=$2
STREAM_KEY=$3

cd /root/churchlab/app

cp ./config.sample.json ./config.json


sed -i "s/##TEAM_ID##/$TEAM_ID/g" ./config.json
sed -i "s/##TEAM_SLUG##/$TEAM_SLUG/g" ./config.json
sed -i "s/##TEAM_KEY##/$STREAM_KEY/g" ./config.json

 
exit 0