#!/bin/bash

echo "Creating new RSA key"

yes y | ssh-keygen -q -t rsa -N '' -f /root/.ssh/id_rsa

echo "New RSA key created"; exit 0