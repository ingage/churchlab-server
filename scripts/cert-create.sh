#!/bin/bash

DOMAIN=$1

if [ -f "/etc/letsencrypt/live/churchlab-relay.streamsprk.com/fullchain.pem" ]; then

certbot delete --cert-name churchlab-relay.streamsprk.com --quiet

fi


if [ ! -f "/etc/letsencrypt/live/$DOMAIN/fullchain.pem" ]; then

certbot certonly --standalone --preferred-challenges http -n -d $DOMAIN -m jay@churchlab.co --agree-tos --quiet

fi

if [ $? -eq 0 ]; then

    sed -i "s/myservername.streamsprk.com/$DOMAIN/g" /root/churchlab/conf/server.conf
    sed -i "s/#server_name/server_name/g" /root/churchlab/conf/server.conf
    sed -i "s/#ssl_certificate/ssl_certificate/g" /root/churchlab/conf/server.conf
    sed -i "s/#ssl_certificate_key/ssl_certificate_key/g" /root/churchlab/conf/server.conf

    echo OK
else
    echo FAIL
fi