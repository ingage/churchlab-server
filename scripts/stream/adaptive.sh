#!/bin/bash


APP=$1
NAME=$2

# /usr/bin/ffmpeg -i rtmp://localhost/$APP/$NAME -vsync cfr \
# -acodec copy -c:v libx264 -b:v 768k -tune zerolatency -preset ultrafast -map_metadata -1 -f flv rtmp://localhost/adaptive/stream_low \
# -acodec copy -c:v libx264 -b:v 2000k -tune zerolatency -preset ultrafast -map_metadata -1 -f flv rtmp://localhost/adaptive/stream_med \
# -c copy -f flv rtmp://localhost/adaptive/stream_src;

/usr/bin/ffmpeg -loglevel -i rtmp://localhost:1935/pub_adaptive/$NAME \
-c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 2500k -f flv -g 30 -r 30 -s 1280x720 -preset superfast -profile:v baseline rtmp://localhost:1935/adaptive/$NAME_720p2628kbs \
-c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 1000k -f flv -g 30 -r 30 -s 854x480 -preset superfast -profile:v baseline rtmp://localhost:1935/adaptive/$NAME_480p1128kbs \
-c:a libfdk_aac -b:a 128k -c:v libx264 -b:v 750k -f flv -g 30 -r 30 -s 640x360 -preset superfast -profile:v baseline rtmp://localhost:1935/adaptive/$NAME_360p878kbs2 >>/tmp/ffmpeg-script.log;