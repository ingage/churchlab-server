#!/bin/bash

if [ -f "/etc/letsencrypt/live/churchlab-relay.streamsprk.com/fullchain.pem" ]; then

certbot delete --cert-name churchlab-relay.streamsprk.com

fi

echo "Renewing certificate"

certbot renew --webroot --webroot-path /var/www/letsencrypt

echo "Restarting nginx"

service nginx restart