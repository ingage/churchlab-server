#!/bin/bash

echo "Moving stunnel config..."; 

cp /root/churchlab/cl-conf/stunnel/stunnel.conf /etc/stunnel/stunnel.conf

echo "Enabling stunnel..."

sed -i "s,ENABLED=0,ENABLED=1,g" /etc/default/stunnel4

echo "Restarting stunnel..."; 

systemctl restart stunnel4 && systemctl status stunnel4

echo "Stunnel configured"; exit 0;